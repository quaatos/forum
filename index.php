<?php
session_start();
require_once('php/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exmin</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="icon" type="image/x-icon" href="images/favicon.png">
</head>
<body>
    <!--Navigation bar-->
    <nav class="navbar">
        <ul>
            <li class="subjects"><a href="subjects/Science/index.php">Science</a></li>
            <li class="subjects"><a href="subjects/nature/index.php">Nature</a></li>
            <li class="subjects"><a href="subjects/memes/index.php">memes</a></li>
            <li class="subjects"><a href="subjects/world/index.php">world</a></li>
            <?php if (isset($_SESSION['user'])) { ?>
                <li style="float: right"><a href="<?php echo "php/account.php?username=" . $_GET = $_SESSION['user']; ?>"><img class="account" src="images/account.png" alt="account image"></li></a>
            <?php } elseif (!isset($_SESSION['user'])) { ?>
                <li style="float: right" class="subjects"><a href="php/login.php">login</a></li>
            <?php } ?>
        </ul>
    </nav>
    <!--Navigation bar-->
							
	
    <h1 class="subjectTitle">Memes</h1>
    <hr>
	<h1 class="subjectTitle">Nature</h1>
    <hr>
    <h1 class="subjectTitle">Science</h1>
    <hr>
	<h1 class="subjectTitle">World</h1>					
    <hr>

    <footer>
        <p>copyright &#169; <?php echo date('Y'); ?> Exmin, all rights reserved. </p>
    </footer>
</body>
</html>