<?php
session_start();
require_once('connection.php');
if (isset($_POST['submit'])) {
    try {
        if (!empty($_POST['username'] && !empty($_POST['password'] && !empty($_POST['repeatPassword'])))) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $repeatPassword = $_POST['repeatPassword'];
            if (strlen($username > 100 || strlen($username) < 2)) {
                if (!preg_match('/^\S.*\s.*\S$/', $username)) { //check for whitespaces in username, email and password
                    if (!preg_match('/^\S.*\s.*\S$/', $_POST['email'])) {
                        if (!preg_match('/^\S.*\s.*\S$/', $_POST['password'])) {
                            if ($repeatPassword !== $password) { //check if the two passwords have a match
                                $error = "<p class='error'>Passwords does not match</p>";
                            } else {
                                $email = $_POST['email'];
                                $EncryptedPassword = password_hash($_POST['password'], PASSWORD_BCRYPT);

                                $sql = "INSERT INTO `users` (`username`, `email`, `password`) VALUES (:username, :email, :password)";
                                $query = $database->prepare($sql);
                                $query->bindParam(':username', $username, PDO::PARAM_STR);
                                $query->bindParam(':email', $email, PDO::PARAM_STR);
                                $query->bindParam(':password', $EncryptedPassword, PDO::PARAM_STR);
                                $query->execute();
                                $_SESSION['user'] = $username;
                                header('location: ../index.php');
                            }
                        } else {
                            $error = "<p class='error'>Password can not contain white spaces!</p>";
                        }
                    } else {
                        $error = "<p class='error'>E-mail can not contain white spaces!</p>";
                    }
                } else {
                    $error = "<p class='error'>Username can not contain white spaces!</p>";
                }
            } else {
                $error = "<p class='error'>Username must be between 2 and 100 characters long!</p>";
            }
        } else {
            $error = "<p class='error'>Please fill in all the fields!</p>";
        }
    } catch (Exception $e) {
        $error = "<p class='error'>This username is already in use!</p>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create exmin account</title>
    <link rel="stylesheet" href="../css/CreateAccount.css">
</head>
<body>
    <!--Login box-->
    <div class="login-box">
        <form action="CreateAccount.php" method="POST" class="login-form">
            <h3>Create Account</h3>
            <hr>
            <input type="text" name="username" placeholder="username">
            <input type="email" name="email" placeholder="E-mail">
            <input type="password" name="password" placeholder="password">
            <input type="password" name="repeatPassword" placeholder="Repeat password">
            <p>Already an account? <a href="login.php">Log in!</a></p>
            <?php if (isset($error)) {echo $error;} ?>
            <hr>
            <input type="submit" name="submit" value="submit">
            <p class="securePassword">We are not responsible for any trouble with your account.<br>Choose your password with care.</p>
        </form>
    </div>
    <!--Login box-->
    
    <footer>
        <p>copyright &#169; <?php echo date('Y'); ?> Exmin, all rights reserved. </p>
    </footer>
</body>
</html>