<?php
//TODO: fix warning if you use username wich doesn't exsist.
session_start();
include_once('connection.php');
try {
    if (isset($_POST['submit'])) {
        if (!empty($_POST['username']) || !empty($_POST['password']) || !empty($_POST['email'])) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            $email = $_POST['email'];
            
            $sql = "SELECT * FROM `users` WHERE `username` = :username";
            $query = $database->prepare($sql);
            $query->bindParam(':username', $username, PDO::PARAM_STR);
            $query->execute();
            $fetch = $query->fetch();
            $fetchUsername = $fetch['username'];
            
            if (strlen($fetchUsername > 0)) {
                $fetchedPassword = $fetch['password'];
                $fetchedEmail = $fetch['email'];
                if ($email == $fetchedEmail) { 
                    if (password_verify($password, $fetchedPassword)) {
                        $_SESSION['user'] = $username;
                        header('location: ../index.php');
                    } else {
                        $error = "<p class='error'>Password is not correct!</p>";
                    }
                } else {
                    $error = "<p class='error'>Incorrect E-mail!</p>";
                }
            } else {
                error_reporting(0);
                $error = "<p class='error'>username does not exsist!</p>";
            }
        } else {
            $error = "<p class='error'>Please fill in all the fields!<p>";
        }
    }
} catch (Exception $e) {
    $error = "<p class='error'>Something went wrong!</p>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login to Exmin</title> 
    <link rel="stylesheet" href="../css/login.css">
</head>
<body>
    <!--Login box-->
    <div class="login-box">
        <form action="login.php" method="POST" class="login-form">
            <h3>login to your account</h3>
            <input type="text" name="username" placeholder="username">
            <input type="email" name="email" placeholder="E-mail">
            <input type="password" name="password" placeholder="password">
            <p>No account? <a href="CreateAccount.php">Create one!</a></p>
            <?php if (isset($error)) {echo $error;} ?>
            <hr>
            <input type="submit" name="submit" value="submit">
        </form>
    </div>
    <!--Login box-->

    <footer>
        <p>copyright &#169; <?php echo date('Y'); ?> Exmin, all rights reserved. </p>
    </footer>
</body>
</html>