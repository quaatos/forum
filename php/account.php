<?php
session_start();
if (empty($_GET['username'])) {
    header('location: ../index.php');
} else {
    $username = $_GET['username'];
}

if ($username !== $_SESSION['user']) { //changing account using the URL is now only possible if the user has logedin once at the target account.
    header('location: logout.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $username ?> | manage account</title>
    <link rel="stylesheet" href="../css/account.css">
</head>
<body>
    <!--Navigation bar-->
    <nav class="navbar">
        <ul>
            <li class="subjects"><a href="../subjects/Science/index.php">Science</a></li>
            <li class="subjects"><a href="../subjects/Nature/index.php">Nature</a></li>
            <li class="subjects"><a href="../subjects/memes/index.php">memes</a></li>
            <li style="float: right"><a href="logout.php">logout</a></li>
        </ul>
    </nav>
    <!--Navigation bar-->

    <footer>
        <p>copyright &#169; <?php echo date('Y'); ?> Exmin, all rights reserved. </p>
    </footer>
</body>
</html>