<?php
session_start();
include_once('../php/connection.php');
if (!empty($_GET['postedby'])) {
    $poster = $_GET['postedby'];
}

if (!empty($_GET['postid'])) {
    $id = $_GET['postid'];
} else {
    echo "failt";
}

if (!empty($_GET['title'])) {
    $title = $_GET['title'];
}
//TODO: tag styling

if ($_GET['referer'] == "meme") {
    $sql = "SELECT * FROM `memes` WHERE `post_id` = :postid";
    $path = "memes";
} elseif ($_GET['referer'] == "nature") {
    $sql = "SELECT * FROM `nature` WHERE `post_id` = :postid";
    $path = "nature";
}


$query = $database->prepare($sql);
$query->bindParam(':postid', $id, PDO::PARAM_INT);
$query->execute();
$fetch = $query->fetch();
$imageName = $fetch['post_file_path'];
$description = $fetch['post_description'];

$tag = $fetch['tags'];
$convToArr = explode(' ', $tag);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/PostDetails.css">
    <title>Post details</title>
</head>
<body>
    <!--Navigation bar-->
    <nav class="navbar">
        <ul>
            <li class="subjects"><a href="../index.php"><img class="home" src="../images/home.png" alt="home"></a></li>
            <li class="subjects"><a href="../subjects/Science/index.php">Science</a></li>
            <li class="subjects"><a href="../subjects/nature/index.php">Nature</a></li>
            <li class="subjects"><a href="../subjects/memes/index.php">memes</a></li>
            <li class="subjects"><a href="../subjects/world/index.php">world</a></li>
            <?php if (isset($_SESSION['user'])) { ?>
                <li style="float: right"><a href="<?php echo "../php/account.php?username=" . $_GET = $_SESSION['user']; ?>"><img class="account" src="../images/account.png" alt="account image"></li></a>
            <?php } elseif (!isset($_SESSION['user'])) { ?>
                <li style="float: right" class="subjects"><a href="php/login.php">login</a></li>
            <?php } ?>
        </ul>
    </nav>
    <!--Navigation bar-->
    <div class="postDetailBox">
        <h1><?php echo $title ?></h1>
        <h3><?php echo "posted by: $poster"; ?></h3>
        <hr>
        <div class="imageBox">
            <img src='<?php echo "../subjects/$path/files/$imageName"?>'>
        </div>
        <hr>
        <?php 
            echo "<div class='description'>$description</div>"; 
            echo "<div class='tagAligment'>";
                echo "<p class='tagInline'>tags: </p>";
                foreach($convToArr as $posttag) {
                    echo "<p class='tag'>" . $posttag . "</p>";
                }
            echo "</div>";
        ?>
        
    </div>

    <footer>
        <p>copyright &#169; <?php echo date('Y'); ?> Exmin, all rights reserved. </p>
    </footer>
</body>
</html>