<?php
//NOTE: all the subject pages are the same, except for the sql query and some css.
require_once('../../php/connection.php');
session_start();

$day = date('F j, ');
$year = date('Y ');
$seconds = date('H:i:s');
$date = $day . $year . $seconds;

if (isset($_SESSION['user'])) {
    $username = $_SESSION['user'];
}

if (isset($_POST['submit'])) {
    if (!empty($_POST['title'])) {
        if (!empty($_POST['tag'])) {
            $title = $_POST['title'];
            $tag = $_POST['tag'];
            if (!empty($_POST['description'])) {
                $description = $_POST['description']; 

                $allowedFiles = ['png', 'jpg', 'jpeg', 'gif', 'mp4', 'jpe'];
                if (is_dir("files/")) { //check if the target directory really is a directory.
                    try {
                        $targetDir = "files/" . basename($_FILES["file"]["name"]);
                        $uploadedFile = $_FILES['file']['tmp_name'];
                        move_uploaded_file($uploadedFile, $targetDir);
                        $filename = $_FILES['file']['name']; //get the filename of the uploaded file
                        $filesize = filesize("files/$filename");
                        if ($filesize < 500000000) { //filesize limit of 500MB
                            $extension = pathinfo($filename, PATHINFO_EXTENSION);
                            if (in_array($extension, $allowedFiles)) {
                                $sql = "INSERT INTO `nature` (`post_title`, `post_file_path`, `posted_by`, `post_date`, `post_description`, `tags`)
                                VALUES (:title, :filename, :posted_by, :post_date, :description, :tags)";

                                $query = $database->prepare($sql);
                                $query->bindParam(':title', $title, PDO::PARAM_STR);
                                $query->bindParam(':filename', $filename, PDO::PARAM_STR);
                                $query->bindParam(':posted_by', $username, PDO::PARAM_STR);
                                $query->bindParam(':post_date', $date, PDO::PARAM_STR);
                                $query->bindParam(':description', $description, PDO::PARAM_STR);
                                $query->bindParam(':tags', $tag, PDO::PARAM_STR);
                                $query->execute();
                                header('location: index.php');
                            } else {
                                $error = "<p class='error'>'." . $extension . "' is not allowed.<br>Allowed files: <b>png, jpg, jpeg, gif, mp4, jpe</b></p>";
                            } 
                        } else {
                            $error = "<p class='error'>File is to large!</p>";
                        }
                    } catch (Exception $e) {
                        $error = "<p class='error'>Unable to post your post! check your internet connection and browser settings and try again!</p>";
                    }
                } else {
                    //send message to server admin
                }
            } else {
                $error = "<p class='error'>Description is required!</p>";
            }
        } else {
            $error = "<p class='error'>Add a tag please!</p>";
        }
    } else {
        $error = "<p class='error'>Title is required</p>";
    }
}
$FetchNatureContentSQL = "SELECT * FROM `nature`";
$stmt = $database->query($FetchNatureContentSQL);
$fetch = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exmin | nature</title>
    <script src="../../javascript/main.js"></script>
    <link rel="stylesheet" href="../../css/post.css">
    <link rel="stylesheet" href="../../css/subject.css">
</head>
<body class="natureBody">
    <!--Navigation bar-->
    <nav class="navbar">
        <ul>
            <li class="subjects"><a href="../../index.php"><img class="home" src="../../images/home.png" alt="home"></li></a>
            <li class="subjects"><a href="../Science/index.php">Science</a></li>
            <li class="subjects"><a href="../nature/index.php" style="color: whitesmoke">Nature</a></li>
            <li class="subjects"><a href="../memes/index.php">memes</a></li>
            <?php if (isset($_SESSION['user'])) { ?>
                <li style="float: right"><a href="<?php echo "php/account.php?username=" . $_GET = $_SESSION['user']; ?>"><img class="account" src="../../images/account.png" alt="account image"></li></a>
            <?php } elseif (!isset($_SESSION['user'])) { ?>
                <li style="float: right" class="subjects"><a href="../../php/login.php">login</a></li>
            <?php } ?>

            <?php
            if (isset($_SESSION['user'])) { ?>
                <li style="float: right" class="subjects"><button onclick="addPost()">post</button></li>
            <?php } ?>
        </ul>
    </nav> 
    <!--Navigation bar-->

    <!--Post box, used to add posts-->
    <?php if(isset($_SESSION['user'])) {?>
        <div id="addPost" class="addNatureBox">
            <h2>Tell us something about nature</h2>
            <a class="close inlineWithTitle" onclick="document.getElementById('addPost').style.visibility = 'hidden';">X</a>
            <hr>
            <form action="index.php" method="POST" class="addForm" enctype="multipart/form-data"> <!--uploading files without enctype is impossible-->
                <input type="text" name="title" placeholder="Title">
                <input type="file" name="file" id="file">

                <textarea name="description" placeholder="post description"></textarea>
                <input type="text" name="tag"  placeholder="tags">
                <?php if(isset($error)) { echo $error; } ?>
                <input type="submit" name="submit" value="submit">
            </form>
        </div>  
    <?php } ?>
    <div class="postbox">            
        <?php foreach ($fetch as $data) { 
            ?>
            <div class='post'>
                <img src='<?php echo "files/" . $data['post_file_path']; ?>' alt='can not show image'>
                
                <div class="postinfo">
                    <p>added: <?php echo $data['post_date'] ?></p>
                    <p>posted by: <?php echo $data['posted_by'] ?></p>
                    <a href="<?php echo "../../php/PostDetails.php?postedby=" . $data['posted_by'] . "&title=" . $data['post_title'] . "&postid=" . $data['post_id'] . "&referer=nature"; ?>">more</a>
                </div>
            </div>
        <?php } ?>
    </div>

    <footer>
        <p>copyright &#169; <?php echo date('Y'); ?> Exmin, all rights reserved. </p>
    </footer>
</body>
</html>