DROP DATABASE IF EXISTS `forum`;
CREATE DATABASE `forum`;
USE `forum`;

CREATE TABLE `users`
(
    user_id int AUTO_INCREMENT PRIMARY KEY,
    username varchar(100) UNIQUE,
    email varchar(100),
    password varchar(255)
);

CREATE TABLE `memes`
(
    post_id int AUTO_INCREMENT PRIMARY KEY,
    post_title varchar(100),
    post_file_path varchar(255),
    posted_by varchar(100) NOT NULL,
    post_date varchar(50) NOT NULL,
    post_description TEXT NOT NULL,
    tags TEXT NOT NULL,
    likes int NOT NULL,
    dislikes int NOT NULL
);

CREATE TABLE `science`
(
    post_id int AUTO_INCREMENT PRIMARY KEY,
    post_title varchar(100),
    post_file_path varchar(255),
    posted_by varchar(100) NOT NULL,
    post_date varchar(50) NOT NULL,
    post_description TEXT NOT NULL,
    tags TEXT NOT NULL,
    likes int NOT NULL,
    dislikes int NOT NULL
);

CREATE TABLE `nature`
(
    post_id int AUTO_INCREMENT PRIMARY KEY,
    post_title varchar(100),
    post_file_path varchar(255),
    posted_by varchar(100) NOT NULL,
    post_date varchar(50) NOT NULL,
    post_description TEXT NOT NULL,
    tags TEXT NOT NULL,
    likes int NOT NULL,
    dislikes int NOT NULL
);
--idea: compare the likes and dislikes. eventualy make a sum to know how many people liked the post